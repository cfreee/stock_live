import React, {Component} from 'react'

type Props = {
    fill: string
}
export function UpTriangle(props: Props = {fill: "rgb(0, 0, 0)"}){
    return (
    <svg viewBox="0,0,100,100">
        <path d="M50,7L0,93L100,93,L50,7" fill={props.fill}/>
    </svg>
    )
}

export function DownTriangle(props: Props = {fill: "rgb(0, 0, 0)"}){
    return (
    <svg viewBox="0,0,100,100">
        <path d="M0,7L50,93L100,7,L0,7" fill={props.fill}/>
    </svg>
    )
}