import React, { Component } from 'react';
import withStyles, { WithStyles } from '@material-ui/core/styles/withStyles';
import { Theme } from '@material-ui/core/styles/createMuiTheme';
import Grid, {GridSpacing} from '@material-ui/core/Grid';
import Panel from './components/Panel'
import Body from './components/Body'
import {Query} from './route/query'

const styles = (theme: Theme) => ({
  root: {
    spacing: 16 as GridSpacing
  },
  container: {
    spacing: 16 as GridSpacing
  },
  panel: {
    margin: theme.spacing.unit,
  }
});

type Props = {
  stockNames: string[]
}

class App extends Component<WithStyles<typeof styles> & Props> {
  query: Query;
  
  constructor(props: WithStyles<typeof styles> & Props){
    super(props)
    this.query = new Query();
  }

  render() {
    const { classes, stockNames } = this.props;

    return (
      <Grid container className={classes.root}>
        <Grid item xs={12}>
          <Grid container justify="center" className={classes.container}>
            <Grid item xs={3} className={classes.panel}>
              <Panel stockNames={stockNames} queryStockData={this.query.queryStock}/>
            </Grid>
            <Grid item xs={7}className={classes.panel}>
              <Body queryData={this.query.queryStockBucket}/>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    )
  }
}

export default withStyles(styles)(App);
