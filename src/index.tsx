import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

const stockFullNames = require('./data/stock_full_list.json')
const stockMissingNames = require('./data/stock_missing_list.json')
const stockNames = stockFullNames.filter((d:string) => !stockMissingNames.includes(d))
ReactDOM.render(<App stockNames={stockNames}/>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
