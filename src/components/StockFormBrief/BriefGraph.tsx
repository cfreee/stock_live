import React, {Component} from 'react';
import * as d3 from 'd3';

type Props = {
    name: string,
    id: number,
    data: (number|undefined)[],
    trending: "up" | "down"
}

class BriefGraph extends Component<Props>{
    width: number = 800;
    height: number = 100;
    componentDidMount(){
        this.drawPath(this.props);
    }

    componentWillReceiveProps(nextProps: Props){
        this.clear();
        this.drawPath(nextProps);
    }

    drawPath(props: Props) {
        const {data, trending, id} = props;
        const min = Math.min.apply(Math, data as number[]);
        const max = Math.max.apply(Math, data as number[]);
        const x = d3.scaleLinear()
                    .domain([0, data.length])
                    .range([0, this.width])
        const y = d3.scaleLinear()
                    .domain([min, max])
                    .range([this.height*0.9, this.height*0.1])
        const lines = (data: (number|undefined)[]): string | undefined => {
            let continuous: boolean = false;
            let path: string = "";
            let cor = "";
            for(let i = 0; i < data.length; i++) {
                if (data[i]){
                    cor = "" + x(i)+","+y(data[i] as number)
                    if(continuous) path += ("L"+cor)
                    else path += ("M"+cor)
                    continuous = true;
                }
                else {
                    continuous = false;
                }
            }
            return path;
        }
        const svg = d3.select("#svg_"+id)
        svg.append("path")
            .attr("d", lines(data) as string)
            .style("stroke", trending == "up" ? "rgb(255, 0, 0)": "rgb(0, 255, 0)")
            .style("stroke-width", 4)
            .style("fill", "none")
    }

    clear() {
        const svg = d3.select("#svg_" + this.props.id);
        svg.selectAll("*").remove();
    }

    render() {
        const {id} = this.props;
        return (
            <div>
                <svg viewBox="0, 0, 800, 100" id={"svg_"+id}> 
                </svg>
            </div>
        )
    }
}

export default BriefGraph;