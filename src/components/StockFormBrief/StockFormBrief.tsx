import React, { Component } from 'react'
import { WithStyles } from '@material-ui/core/styles/withStyles'
import { withStyles, createStyles } from '@material-ui/core/styles'
import { Theme } from '@material-ui/core/styles/createMuiTheme'
import Grid from '@material-ui/core/Grid'
import BriefGraph from './BriefGraph'
import { UpTriangle, DownTriangle } from '../../icons/Triangle'
import {StockInfo} from '../../route/query'


const styles = (theme: Theme) => createStyles({
  container: {
    marginTop: theme.spacing.unit * 2,
    paddingTop: theme.spacing.unit * 0.5,
    border: "solid",
    borderRadius: "10px",
    borderWidth: 1,
    borderColor: "#ccc",
  },

  textContainer: {
    position: 'relative',
    margin: theme.spacing.unit,
  },
  price: {
    fontSize: "1.5vw",
    marginLeft: theme.spacing.unit * 0.5,
    position: "absolute",
    bottom: 0,
  },
  ratio: {
    fontSize: "1.4vw",
    color: 'rgb(255, 255, 255)',
    position: "absolute",
    margin: theme.spacing.unit*0.5,
    bottom: 0,
    textAlign: "center",
  },
  ratioContainer: {
    borderRadius: "10px",
    marginLeft: theme.spacing.unit,
    padding: theme.spacing.unit,
    alignItems: "center",
    position: "relative",
  },
  name: {
    fontSize: "2.0vw",
    marginLeft: theme.spacing.unit * 0.5,
    position: "absolute",
    bottom: 0,
  },
  iconContainer: {
    width: "10vw",
  },
})


type States = {

}

type Props = {
  id: number,
  name: string,
  data: StockInfo
}

class StockFormBrief extends Component<Props & WithStyles<typeof styles>, States>{

  constructor(props: Props & WithStyles<typeof styles>){
    super(props)
    this.handleDragStart = this.handleDragStart.bind(this);
  }

  handleDragStart(event: any) {
    event.dataTransfer.setData('id',this.props.name)
  }

  handleDrag(event: any) {
  }

  handleDragEnd(event: any) {
  }

  render() {
    const { classes, name, id} = this.props;
    const { currentPrice, changingRate, data} = this.props.data
    return (

      <div draggable={true} className={classes.container} id={name}
        onDragStart={this.handleDragStart} onDrag={this.handleDrag} onDragEnd={this.handleDragEnd}>
        <Grid container className={classes.textContainer}>
          <Grid item xs={4}>
            <text className={classes.name}> {name} </text>
          </Grid>
          <Grid item xs={3}>
            <text className={classes.price}> {currentPrice.toFixed(2)}</text>
          </Grid>
          <Grid item xs={1} className={classes.iconContainer}>
            {Number(changingRate) > 0 ? <UpTriangle fill="rgb(255, 0, 0)" /> : <DownTriangle fill="rgb(0,255,0)" />}
          </Grid>
          <Grid item xs={3} className={classes.ratioContainer}
            style={{ background: Number(changingRate) > 0 ? 'rgb(255,0,0)' : 'rgb(0,255,0)' }} >
            <text className={classes.ratio}>
              {(Number(changingRate) > 0 ? '+' : '') + changingRate.toFixed(2)+'%'}
            </text>
          </Grid>
        </Grid>
        <BriefGraph
          data={data}
          trending={Number(changingRate) > 0 ? "up" : "down"}
          name={name}
          id={id}
        />
      </div>
    );
  }
}

export default withStyles(styles)(StockFormBrief);