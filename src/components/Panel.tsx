import React, { Component } from 'react';
import Paper from '@material-ui/core/Paper';
import { WithStyles } from '@material-ui/core/styles/withStyles';
import { withStyles, createStyles } from '@material-ui/core/styles';
import { Theme } from '@material-ui/core/styles/createMuiTheme';
import Input from '@material-ui/core/Input'
import { fade } from '@material-ui/core/styles/colorManipulator';
import SearchIcon from '@material-ui/icons/Search';
import {QueryStock, StockInfo} from '../route/query'
import StockFormBrief from './StockFormBrief/StockFormBrief'

const styles = (theme: Theme) => createStyles({
  paper: {
    height: '100vh',
  },
  extendedIcon: {
    marginRight: theme.spacing.unit,
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    borderWidth: 1,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing.unit,
      width: 'auto',
    },
    margin: theme.spacing.unit,
  },
  searchIcon: {
    width: theme.spacing.unit * 9,
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
    width: '100%',
  },
  inputInput: {
    paddingTop: theme.spacing.unit,
    paddingRight: theme.spacing.unit,
    paddingBottom: theme.spacing.unit,
    paddingLeft: theme.spacing.unit * 10,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: 120,
      '&:focus': {
        width: 200,
      },
    },
  },

  contrainer: {
    paddingTop: theme.spacing.unit,
    paddingRight: theme.spacing.unit,
    paddingBottom: theme.spacing.unit,
    paddingLeft: theme.spacing.unit,
  }
});

export type QueryStockData = (id: string) => StockInfo | undefined;

type Props = {
  stockNames: string[],
  queryStockData: QueryStock
}

type State = {
  stockList: string[]
}

class Panel extends Component<WithStyles<typeof styles> & Props, State> {

  state = {
    stockList: ["AAP", "ADBE", "AMZN", "FB", "GOOG", "GOOGL"]
  };

  handleSearch = (event: any) => {
    const {stockList} = this.state
    if (event.keyCode == 13) {
      const value = event.target.value;
      if (this.props.stockNames.includes(value)) {
        const index = stockList.indexOf(value);
        if (index <= -1){
          const queryResult = this.props.queryStockData(value);
          if (typeof queryResult == "undefined") {
            // alert("invalid query")
            console.log("invalid query")
          }
          else {
            stockList.unshift(value);
            this.setState({stockList: stockList})
          }
          console.log(stockList)
        }
        else if (index > 0) {
          const updatedStock = stockList[index];
          for (let i = index; i > 0; i--) {
            stockList[i] = stockList[i-1];
          }
          stockList[0] = updatedStock;
          this.setState({stockList: stockList})
          console.log(stockList)
        }
        // index == 0 => no need to update
      }
    }
  }

  render() {
    const { classes } = this.props;
    const {stockList} = this.state;
    return (
      <Paper className={classes.paper}>
        <div className={classes.search}>
          <div className={classes.searchIcon}>
            <SearchIcon />
          </div>
          <Input
            placeholder="Search…"
            classes={{
              root: classes.inputRoot,
              input: classes.inputInput,
            }}
            onKeyDown={this.handleSearch}
            
          />
        </div>
        <div className={classes.contrainer}>
        {
          stockList.map((d, i) => 
            (<StockFormBrief 
              name={d} 
              id={i}
              data={this.props.queryStockData(d)}
              key={i}
              />))
        }
        </div>
      </Paper>
    )
  }
}




export default withStyles(styles)(Panel);
