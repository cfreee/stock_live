import React, {Component} from 'react'
import {withStyles, createStyles} from '@material-ui/core/styles'
import {WithStyles} from '@material-ui/core/styles/withStyles'
import { Theme } from '@material-ui/core/styles/createMuiTheme';
import Grid from '@material-ui/core/Grid'
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider'
import Chip from '@material-ui/core/Chip'
import ExtendGraph, {HeritStates} from './ExtendedGraph'
import {QueryStockBucket} from '../../route/query'
import * as d3 from 'd3'

const styles = (theme: Theme) => createStyles({
  paper: {
    
  },
  chip: {
    margin: theme.spacing.unit,
  },
  chipContainer: {
    height: '5vh',
    margin: theme.spacing.unit,
  },
  chartContainer: {
    height: '50vh',
    margin: theme.spacing.unit,
  }
})

type Props = {
  id: string,
  queryData: QueryStockBucket,
}

type States = HeritStates;

class StockFormExpanded extends Component<Props & WithStyles<typeof styles>, States>{

  constructor(props: Props & WithStyles<typeof styles>){
    super(props);

    // For test
    if (this.props.id == "test0"){
      this.state = {
        stockList: ["AAP", "AMZN"],
        displayMode: 0
      };
    }
    else if (this.props.id == "test1"){
      this.state = {
        stockList: ["GOOG", "GOOGL"],
        displayMode: "Compare"
      };
    }
    else {
      this.state = {
        stockList: [],
        displayMode: "None"
      };
    }
    this.handleDrop = this.handleDrop.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }
  handleDelete = (id: string) => (event: any) => {
    let {stockList, displayMode} = this.state;
    const index = stockList.indexOf(id);
    if (index < 0) return;
    stockList.splice(index, 1)
    if (stockList.length == 0){
      displayMode = "None";
    }
    else if (index == displayMode){
      displayMode = "Compare";
    }
    this.setState({stockList: stockList, displayMode: displayMode});
  }

  handleClick = (id: string) => (event: any) => {
    let {stockList, displayMode} = this.state;
    const index = stockList.indexOf(id);
    if (index < 0) return;
    if (displayMode == index) {
      displayMode = "Compare";
    }
    else {
      displayMode = index;
    }
    this.setState({stockList: stockList, displayMode: displayMode});
  }

  handleDrop(event: any){
    const newId = event.dataTransfer.getData('id');
    const {stockList} = this.state;
    let {displayMode} = this.state;
    if (!stockList.includes(newId) && stockList.length < 5){
      stockList.push(newId as string);
      if (displayMode == "None"){
        displayMode = "Compare";
      } 
      this.setState({stockList: stockList, displayMode: displayMode});
    }
  }

  handleDragEnter(event: any){}

  handleDragOver(event: any){
    event.preventDefault();
  }

  render(){
    const {classes, id} = this.props;
    const {stockList} = this.state;
    return (
      
        <Paper onDrop={this.handleDrop} onDragEnter={this.handleDragEnter} onDragOver={this.handleDragOver}>
          <div className={classes.chipContainer}>
          {
            stockList.map(d => (<Chip label={d} className={classes.chip}
              onDelete={this.handleDelete(d)} 
              onClick={this.handleClick(d)} 
              color={this.state.displayMode == stockList.indexOf(d)? "secondary": "primary"}/>))
          }
          </div>
          <Divider />
          <div className={classes.chartContainer}>
            <ExtendGraph id={id} heritState={this.state} queryData={this.props.queryData}/>
          </div>
        </Paper>
      
    )
  }
}

export default withStyles(styles)(StockFormExpanded)