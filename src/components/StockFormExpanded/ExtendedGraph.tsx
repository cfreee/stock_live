import React, { Component } from 'react';
import * as d3 from 'd3';
import { BacketStockInfo, QueryStockBucket } from '../../route/query'
import { Drawer } from '@material-ui/core';


export type HeritStates = {
    stockList: string[],
    displayMode: "Compare" | "None" | number,
}

type Props = {
    id: string,
    heritState: HeritStates,
    queryData: QueryStockBucket
}

class ExtendedGraph extends Component<Props>{

    private height: number = 200;
    private width: number = 400;
    private svgId: string;
    private data: BacketStockInfo;
    private color: any;
    private scales: {
        X: any,
        date: any,
        priceY: any[],
        volumeY: any[],
        commonPriceY: any,
        commonVolumeY: any,
    };

    constructor(props: Props) {
        super(props);
        this.state = Object(props.heritState);
        this.scales = {
            X: null,
            date: null,
            priceY: [],
            commonPriceY: null,
            volumeY: [],
            commonVolumeY: null
        };
        this.svgId = "extended_graph_svg_" + this.props.id;
        this.data = this.props.queryData(this.props.heritState.stockList)
        this.color = d3.scaleOrdinal(d3.schemeCategory10);
    }

    componentDidMount() {
        this.draw(this.props);
    }

    componentWillReceiveProps(nextProps: Props) {
        this.draw(nextProps);
    }

    query(stockList: string[]) {
        const { queryData } = this.props;
        this.data = queryData(stockList);
    }

    draw(props: Props) {
        const { displayMode, stockList } = props.heritState
        this.clear();
        this.query(stockList);
        if (displayMode == "None") {
            return;
        }
        else if (displayMode == "Compare") {
            this.drawCompare();
        }
        else {
            return this.drawDetail(displayMode as number);
        }
    }

    clear() {
        const svg = d3.select("#" + this.svgId);
        svg.selectAll("*").remove();
    }

    updateScale() {
        const data = this.data;
        const days = data.date.length;
        if (0 == days || 0 == data.stocks.length) {
            return;
        }
        const priceMaxes = data.stocks.map(d => Math.max.apply(Math, d.detailed.map(d => d.high as number)));
        const globalPriceMax = Math.max.apply(Math, priceMaxes);
        const priceMins = data.stocks.map(d => Math.min.apply(Math, d.detailed.map(d => d.low as number)));
        const globalPriceMin = Math.min.apply(Math, priceMins);
        const volumeMaxes = data.stocks.map(d => Math.max.apply(Math, d.detailed.map(d => d.volume as number)));
        const globalvolumeMax = Math.max.apply(Math, volumeMaxes);
        this.scales.X = d3.scaleLinear()
            .domain([0, days])
            .range([this.width * 0.0, this.width * 0.95])
        this.scales.date = d3.scaleBand()
            .domain(data.date.map(d => d.substring(5, 10)))
            .range([this.width * 0.0, this.width * 0.95])
        this.scales.priceY = d3.range(0, days).map(d => d3.scaleLinear()
            .domain([priceMins[d] * 0.9, priceMaxes[d]])
            .range([this.height * 0.6, this.height * 0.0]))
        this.scales.commonPriceY = d3.scaleLinear()
            .domain([globalPriceMin * 0.9, globalPriceMax])
            .range([this.height * 0.6, this.height * 0.0])
        this.scales.volumeY = volumeMaxes.map(d => d3.scaleLinear()
            .domain([0, d])
            .range([this.height * 0.9, this.height * 0.7]))
        this.scales.commonVolumeY = d3.scaleLinear()
            .domain([0, globalvolumeMax])
            .range([this.height * 0.9, this.height * 0.7])
    }

    drawAxis(mode: "Compare" | number) {
        const { X, commonPriceY, commonVolumeY,priceY, volumeY, date } = this.scales;
        const svg = d3.select('#' + this.svgId);
        if (mode == "Compare"){
            svg.append("g").call(d3.axisLeft(commonPriceY).ticks(8).tickSize(2)).style("font-size", "6");
            svg.append("g").call(d3.axisLeft(commonVolumeY).ticks(5).tickSize(2)).style("font-size", "6");
        }
        else if (typeof mode == "number"){
            svg.append("g").call(d3.axisLeft(priceY[mode]).ticks(8).tickSize(2)).style("font-size", "6");
            svg.append("g").call(d3.axisLeft(volumeY[mode]).ticks(5).tickSize(2)).style("font-size", "6");
        }
        svg.append("g").call(d3.axisBottom(date)
            .tickValues(date.domain().filter((d: any, i: number) => (!(i % 5)))))
            .style("font-size", "6")
            .attr("transform", "translate(0, " + 0.6 * this.height + ")");
        svg.append("g").call(d3.axisBottom(date)
            .tickValues(date.domain().filter((d: any, i: number) => (!(i % 5)))))
            .style("font-size", "6")
            .attr("transform", "translate(0, " + 0.9 * this.height + ")");
        svg.append("text")
            .text("Price /USD")
            .attr("transform", "translate(-20, -2)")
            .style("font-size", "6");
        svg.append("text")
            .text("Volume /K")
            .attr("transform", "translate(-20, " + (0.7 * this.height-2) + ")")
            .style("font-size", "6")
    }

    drawCompare() {
        this.updateScale();
        const { X, commonPriceY, commonVolumeY} = this.scales;
        const data = this.data.stocks;
        const stockNum: number = data.length;
        const barWidth: number = (X(1) - X(0)) / stockNum * 0.95;
        const svg = d3.select('#' + this.svgId);
        this.drawAxis("Compare");
        const lineGenerator = (data: (number | undefined)[]): string | undefined => {
            let continuous: boolean = false;
            let path: string = "";
            let cor = "";
            for (let i = 0; i < data.length; i++) {
                if (data[i]) {
                    cor = "" + X(i) + "," + commonPriceY(data[i] as number)
                    if (continuous) path += ("L" + cor)
                    else path += ("M" + cor)
                    continuous = true;
                }
                else {
                    continuous = false;
                }
            }
            return path;
        }
        const lines = svg.selectAll(".line")
            .data(data)
            .enter()
            .append("path")
            .attr("class", "line")
            .attr("d", d => lineGenerator(d.data) as string)
            .style("stroke", (d, i) => this.color(i))
            .style("fill", "none")

        const bars = svg.selectAll(".bar_g")
            .data(data)
            .enter()
            .append("g")
            .attr("class", "line_g")
        bars.selectAll(".bar")
            .data((d, i) => d.detailed.map(d => [i, d.volume]))
            .enter()
            .append("rect")
            .attr("class", "bar")
            .attr("x", (d, i) => X(i) + Number(d[0]) * barWidth)
            .attr("y", d => commonVolumeY(d[1]))
            .attr("width", barWidth)
            .attr("height", d => commonVolumeY(0) - commonVolumeY(d[1]))
            .style("fill", d => this.color(d[0]))
            .style("opacity", 0.6)
            .transition()
            .duration(500)
    }

    drawDetail(index: number) {
        this.updateScale();
        const data = this.data;
        this.drawAxis(index);
        const { X, priceY, volumeY, date } = this.scales;
        const svg = d3.select("#" + this.svgId);
    
        const kindle = svg.selectAll(".kindle")
            .data(data.stocks[index].detailed)
            .enter()
            .append("g")
            .attr("class", "kindle")
        kindle.append("line")
            .attr("y1", d => priceY[index](d.high as number))
            .attr("y2", d => priceY[index](d.low as number))
            .attr("x1", (d, i) => (X(i) + X(i + 1)) / 2)
            .attr("x2", (d, i) => (X(i) + X(i + 1)) / 2)
            .style("stroke", d => (Number(d.open) < Number(d.close)) ? "red" : "green")
            .style("opacity", 0.8)
        kindle.append("rect")
            .attr("x", (d, i) => (X(i) * 0.9 + X(i + 1) * 0.1))
            .attr("y", d => priceY[index](Math.max(Number(d.open), Number(d.close))))
            .attr("width", (d, i) => (X(i + 1) - X(i)) * 0.8)
            .attr("height", d => Math.abs(priceY[index](Number(d.open)) - priceY[index](Number(d.close))))
            .style("fill", d => (Number(d.open) < Number(d.close)) ? "red" : "green")
            .style("opacity", 0.8)

        const bars = svg.selectAll(".bars")
            .data(data.stocks[index].detailed)
            .enter()
            .append("g")
            .attr("class", "bars");

        bars.append("rect")
            .attr("x", (d, i) => (X(i) * 0.9 + X(i + 1) * 0.1))
            .attr("y", volumeY[index](0))
            .attr("height", 0)
            .attr("width", (d, i) => (X(i + 1) - X(i)) * 0.8)
            .style("fill", "rgb(23, 138, 221)")
            .style("opacity", 0.6)
            .transition()
            .duration(500)
            .attr("y", d => volumeY[index](Number(d.volume)))
            .attr("height", d => (volumeY[index](0) - volumeY[index](Number(d.volume))))
    }

    render() {
        const { id } = this.props;
        return (
            <div>
                <svg viewBox={"-50, -10, " + (this.width + 50) + ", " + (this.height+10)} id={this.svgId}>
                </svg>
            </div>
        )
    }
}

export default ExtendedGraph;