import React, { Component } from 'react'
import { withStyles, createStyles } from '@material-ui/core/styles'
import { WithStyles } from '@material-ui/core/styles/withStyles'
import { Theme } from '@material-ui/core/styles/createMuiTheme';
import StockFormExpanded from './StockFormExpanded/StockFormExpanded'
import {QueryStockBucket} from '../route/query'

const styles = (theme: Theme) => createStyles({
  paper: {
    height: '100vh',
  },
})

type Props = {
  queryData: QueryStockBucket
}

type States = {
  windowLists: string[]
}

class Body extends Component<Props & WithStyles<typeof styles>, States>{
  states = {windowLists: ["test0", "test1"]}
  render() {
    const { windowLists } = this.states;
    return windowLists.map((d, i) =>
      (<StockFormExpanded id={d} queryData={this.props.queryData} key={i}/>)
    )
  }
}

export default withStyles(styles)(Body)