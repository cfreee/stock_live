import * as d3 from 'd3'

export type QueryDurationType = "IntraDay" | "Daily" | "Monthly"

var najax = require('najax');

export type DetailedData = {
    open: number | undefined,
    high: number | undefined,
    low: number | undefined,
    close: number | undefined,
    volume: number | undefined
}

export type StockInfo = {
    id: string,
    date: string[],
    data: number[],
    currentPrice: number,
    lastDayClosePrice: number,
    changingRate: number,
    detailed: DetailedData[]
}

export type BacketStockInfo = {
    date: string[],
    stocks: StockInfo[]
}

export type QueryStockBucket = (idList: string[]) => BacketStockInfo
export type QueryStock = (id: string) => StockInfo

const sleep = (milliseconds: number) => {
    return new Promise(resolve => setTimeout(resolve, milliseconds))
}

export class Query {

    private cacheBacket: StockInfo[] = [];
    constructor() {
        this.cacheBacket = [
            require('../data/cache/AAP.json'),
            require('../data/cache/ADBE.json'),
            require('../data/cache/AMZN.json'),
            require('../data/cache/FB.json'),
            require('../data/cache/GOOG.json'),
            require('../data/cache/GOOGL.json')
        ];
        this.queryStock = this.queryStock.bind(this);
        this.queryStockBucket = this.queryStockBucket.bind(this);
    }

    private _makeup_data(id: string): StockInfo {
        let value: StockInfo = {
            id: id,
            data: d3.range(0, 30).map(d => Math.random()),
            date: d3.range(1, 32, 1).map(d => "2017-12-" + d).concat(d3.range(1, 30, 1).map(d => "2018-01-" + d)),
            currentPrice: Math.random() * 10 + 100,
            lastDayClosePrice: Math.random() * 10 + 100,
            changingRate: (Math.random() - 0.5) * 10,
            detailed: [],
        };
        let open = 300;
        for (let i = 0; i < 60; i++) {
            let close = open + (Math.random() - 0.5) * 50;
            value.detailed.push({
                'open': open,
                'close': close,
                'high': Math.max(open, close) + 10 * Math.random(),
                'low': Math.min(open, close) - 10 * Math.random(),
                'volume': 10000000 * Math.random() + 10000000
            })
            open = close;
        }

        return value;
    }
    private createCORSRequest(method: 'GET' | 'POST', url: string) {
        let xhr = null;
        xhr = new XMLHttpRequest();
        if ("withCredentials" in xhr) {
            // XHR for Chrome/Firefox/Opera/Safari.
            xhr.open(method, url, true);
        } else {
            // CORS not supported.
            xhr = null;
        }
        return xhr;
    }


    private _sync_fetchData(id: string): any {
        const promise = new Promise((resolve, reject) => {
            const url = 'http://localhost:5000/stock?stockId=' + id;
            const xhr = this.createCORSRequest('GET', url)
            if (!xhr) {
                alert('CORS not supported');
                return;
            }
            xhr.onload = function () {
                var text = xhr.responseText;
                resolve(text);
            };
            xhr.onerror = function () {
                console.log('Woops, there was an error making the request.');
            };
            xhr.send();
        });
        return promise
    }

    private async _addDataToBin(id: string){
        const data = await this._sync_fetchData(id);
        this.cacheBacket.push(data as StockInfo);
    }

    private _sync_fetch_data(id: string){
        const request = require('sync-request');
        try{
            const res = request('GET', 'http://localhost:5000/stock?stockId=' + id);
            return JSON.parse(res.getBody());
        }
        catch(error){
            alert("There was an error making the request. Please check whether the server run properly");
        }
        
    }

    queryStock(id: string, mode: QueryDurationType = "Monthly"): StockInfo {
        
        if (this.cacheBacket.map(d => d.id).includes(id)) {
            return this.cacheBacket[this.cacheBacket.map(d => d.id).indexOf(id)];
        }
        else {
            this.cacheBacket.push(this._sync_fetch_data(id) as StockInfo);
            return this.cacheBacket[this.cacheBacket.map(d => d.id).indexOf(id)];
        }
    }

    queryStockBucket(idList: string[]): BacketStockInfo {
        const stockInfoList = idList.map(d => this.queryStock(d))
        const date = stockInfoList.length > 0 ? stockInfoList[0].date : [];
        return { date: date, stocks: stockInfoList };
    }
}