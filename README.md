# Stock Live

Stock live is a minimalist system to present and help to compare S&P 500 stock prices and volumes. This system is for HKUST VisLab coding test 2019. The system consists of both back-end part implemented in Python with [Flask](http://flask.pocoo.org/), [MongoDB](https://www.mongodb.com/download-center/community) and front-end part implemented in Typescript as a [node.js](https://nodejs.org/zh-cn/) project with [React](https://reactjs.org/), [webpack](https://webpack.js.org/), and [d3](https://d3js.org/). This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

![stock_live](./archive/stock_live.png)

## How to run

After cloning the repo and move to the root directory

**install the requirements**

```
npm install
```

**run the app in the development mode**

```
npm start
```

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

**build the app for production** 

```
npm run build
```

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## How to use

The interface of the system has two part: **Panel** and **Body**(currently contains two windows). 

Panel lays on the left part with stock cards providing brief infomation of the stock: Id,  current price(last update time), changing ratio from the last trading day to current, and the trend in the last 60 trading days presented in a line chart. After entering the valid stock id to the "Search" input in the top, the stock card list will be renewed. 

Body is on the right parts containing two windows. To get detailed information of the stock price & volume trends, you can drag the stock card in the Panel to either of the two windows. The action will be ended with the appendance of a chip named as the stock id to the header of the window. If you want to remove the chip, just click the delete button. 

Window has two display modes: **compare mode** as default to present the stock price trend in line chart and volumes in bar chart, **detailed mode** triggered in by clicking a chip of the header and triggered out by clicking it again, which presents the prices in **candlestick chart**.

## Things to pay attention to

If you want to keep the front-end able to send request to back-end and get valid answer, please make sure that you run the back-end server properly, otherwise, there will be an alert. Please refer to **[stock live backend README](./stock_live_back_end/README.md)** to run the backend server. Since the database is on the server of our group, I preserved some data in cache and the system will be initialized as above.